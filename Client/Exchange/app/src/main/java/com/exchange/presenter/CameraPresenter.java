package com.exchange.presenter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceView;

import com.exchange.model.camera.ExchangeCamera;
import com.exchange.model.camera.OrientationUtils;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by eitan on 17/01/2017.
 */

public class CameraPresenter {
    private static final int SNAPSHOT_MAX_EDGE_PIXELS = 300;

    private ExchangeCamera mExchangeCamera;
    private UserPictureListener mUserPictureListener;
    private Camera.PictureCallback mPictureCallback;
    private boolean mSquareBitmapRequested;

    public CameraPresenter(SurfaceView cameraSV, boolean squareBitmapRequested) {
        mSquareBitmapRequested = squareBitmapRequested;
        mExchangeCamera = new ExchangeCamera(cameraSV);
        mPictureCallback = getPictureCallback();
    }

    public void takePicture(UserPictureListener takePictureListener){
        mUserPictureListener = takePictureListener;
        mExchangeCamera.takePicture(mPictureCallback);
    }

    private Camera.PictureCallback getPictureCallback() {
        final OrientationUtils orientationUtils = new OrientationUtils();

        Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                if (data == null){
                    String errorString = "Error: Picture taken but PictureCallback failed to get data array.";
                    mUserPictureListener.onError(errorString);
                    return;
                }

                //make a Bitmap
                Bitmap pictureBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

                //determine orientation of picture meta-data
//                int pictureOrientation = orientationUtils.getOrientationFromMetaData(data);
//                Log.d(TAG, "getOrientationFromMetaData found " + pictureOrientation);
//
//                //if no relevant picture meta-data found, try fitting to device orientation-correction int
//                if (pictureOrientation == ExchangeCamera.NO_ORIENTATION_DATA){
//                    pictureOrientation = orientationUtils.getOrientationFromDeviceOrientation(camera);
//                    Log.d(TAG, "getOrientationFromDeviceOrientation found " + pictureOrientation);
//                }

                //determine orientation of picture meta-data
                int pictureOrientation = orientationUtils.getOrientationFromDeviceOrientation(camera);
                Log.d(TAG, "getOrientationFromDeviceOrientation found " + pictureOrientation);

                //cut bitmap to square if needed
                if (mSquareBitmapRequested){
                    pictureBitmap = cutSquareBitmap(pictureBitmap);
                }

                //rotate picture if necessary
                if (pictureOrientation != 0) {
                    Log.d(TAG, "Rotating bitmap by " + pictureOrientation);
                    pictureBitmap = orientationUtils.rotateBitmap(pictureBitmap, pictureOrientation);
                }

                //resize result bitmap to predetermined size
                pictureBitmap = scaleBitmap(pictureBitmap);


                //return picture bitmap to view
                if (mUserPictureListener != null){
                    mUserPictureListener.onPictureTaken(pictureBitmap);
                }

                //reset preview view
                mExchangeCamera.setPreview();
            }
        };

        return pictureCallback;
    }

    private Bitmap cutSquareBitmap(Bitmap pictureBitmap) {
        Bitmap cutBitmap;
        int pictureWidth = pictureBitmap.getWidth();
        int pictureHeight = pictureBitmap.getHeight();
        int newWidth = (pictureHeight > pictureWidth) ? pictureWidth : pictureHeight;
        int newHeight = (pictureHeight > pictureWidth)? pictureHeight - ( pictureHeight - pictureWidth) : pictureHeight;
        int cropW = (pictureWidth - pictureHeight) / 2;
        cropW = (cropW < 0)? 0: cropW;
        int cropH = (pictureHeight - pictureWidth) / 2;
        cropH = (cropH < 0)? 0: cropH;
        cutBitmap = Bitmap.createBitmap(pictureBitmap, cropW, cropH, newWidth, newHeight);

        return cutBitmap;
    }

    private Bitmap scaleBitmap(Bitmap pictureBitmap) {
        int originalWidth = pictureBitmap.getWidth();
        int originalHeight = pictureBitmap.getHeight();
        double shorterEdge;

        if (originalWidth >= originalHeight){
            shorterEdge = (SNAPSHOT_MAX_EDGE_PIXELS / (double)originalWidth) * originalHeight;
            pictureBitmap = Bitmap.createScaledBitmap(pictureBitmap, SNAPSHOT_MAX_EDGE_PIXELS, (int)shorterEdge, false);
        } else {
            shorterEdge = (SNAPSHOT_MAX_EDGE_PIXELS / (double)originalHeight) * originalWidth;
            pictureBitmap = Bitmap.createScaledBitmap(pictureBitmap, (int)shorterEdge, SNAPSHOT_MAX_EDGE_PIXELS, false);
        }

        return pictureBitmap;
    }

    public interface UserPictureListener {
        void onPictureTaken(Bitmap pictureBitmap);
        void onError(String error);
    }

}
