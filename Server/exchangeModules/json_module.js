var http = require('http');

exports.getItem = function (req, res, filepath) {
	var fileSystem = require('fs');
	fileSystem.readFile(filepath, 'utf8', function(err, contents) {
		console.log("fileSystem | Sending file contents to user (" + filepath + ")");
		res.setHeader('Content-Type', 'application/json');
        	res.send(contents);        
		
		//The following returns the JSON encased with String parenthisis 	
		//res.send(JSON.stringify(contents)); //returns the json in a string format
	});
}
