package com.exchange.daggertest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.exchange.R;

import javax.inject.Inject;

public class DaggerActivity extends AppCompatActivity implements View.OnClickListener{
    @Inject
    public TestTextComponent mTextComponent;

    private TextView mDaggerTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dagger);

        mDaggerTV = (TextView) findViewById(R.id.dagger_tv);

        //dagger
        mTextComponent = DaggerTestTextComponent.builder() //Dagger recognizes components on *compilation time* so some unrecognized classes might get recog. once pressing 'play' for run.
                .textModule1(new TextModule1())
                .textModule2(new TextModule2())
                .build();
        mTextComponent.inject(this);
    }

    @Override
    public void onClick(View v) { ! //Why doesn't dagger run each modules separate method? (The expected text is Dagger text 1' OR '2: Demo Text.")
        switch (v.getId()) {
            case R.id.dagger1_btn:
                String testText1 = mTextComponent.provideTestText1("Demo text.");
                mDaggerTV.setText(testText1);
                break;
            case R.id.dagger2_btn:
                String testText2 = mTextComponent.provideTestText2("Demo text.");
                mDaggerTV.setText(testText2);
                break;
        }
    }

    ! //Also, once this is answered, how to combine dagger and MVP?
}
