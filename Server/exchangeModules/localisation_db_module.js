//***************//
//***** API *****//
//***************//
var http = require('http');

exports.viewLocalisationTable = function (req, res) {
	console.log("db_module | viewLocalisationTable called.");
	
	mySqlConnection.query('SELECT * from localisation_table', function(err, rows, fields) {		
		  if (!err) {
		    console.log('The solution is: ', rows);
		  } else {
		    console.log('Error while performing viewLocalisationTable. err = ' + err);
		  }
		
		res.send(rows);
	});
}

exports.getLocalisationFromTable = function (req, res) {
	console.log("db_module | getLocalisationFromTable called.");
	var language = req.query.language;

    var mySqlQuery = 'SELECT * FROM `itemsdb`.`localisation_table` WHERE language = \'' + language
      + '\' ORDER BY language';
    console.log(mySqlQuery);

	mySqlConnection.query(mySqlQuery, function(err, rows, fields) {		
		  if (err) {		  
		  	var errString = 'Error while performing getLocalisationFromTable. err = ' + err;
		    console.log(errString);
		    res.status(400); //bad request
		    res.send(errString);
		  }
		
		res.send(rows);
	});

}

//*************************//
//***** MySql - Setup *****//
//*************************//
var LOCAL_SQL_SERVER_PORT = 3306;
var LOCAL_SQL_SERVER_HOST = 'localhost';
var LOCAL_SQL_SERVER_USER = 'root';
var LOCAL_SQL_SERVER_PASS = 'mypass15';
var LOCAL_SQL_SERVER_DATABASE = 'itemsdb';


var mySql = require('mysql')
var mySqlConnection = mySql.createConnection({
	port: LOCAL_SQL_SERVER_PORT,  //should either be the default port (3306) or the same port as the one our app.js is listening to (3000)
	host: LOCAL_SQL_SERVER_HOST,
	user: LOCAL_SQL_SERVER_USER,
	password: LOCAL_SQL_SERVER_PASS,
	database: LOCAL_SQL_SERVER_DATABASE
});

connectionStartHandleErrors();


//**************************************//
//***** MySql - Connection Helpers *****//
//**************************************//

function connectionStartHandleErrors(){
	mySqlConnection.connect(function(err){
		if (err){
			console.log("MySql connection failed. err = " + err);
			console.log("Did you remember to activate MySql server from System Preferences?");
		} else {
			console.log("MySql (Localisation) connection succesful.");
		}
	});
}

function connectionEndHandleErrors(){
	mySqlConnection.end();
	console.log('Connection closed.'); //TODO: i'm not sure connection really closes :/ verify that later on..
}

exports.endMySqlConnection = connectionEndHandleErrors;