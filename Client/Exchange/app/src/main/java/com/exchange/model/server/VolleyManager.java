package com.exchange.model.server;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by eitan on 16/01/2017.
 */

public class VolleyManager {
    private static VolleyManager sInstance = null;
    private RequestQueue mRequestQueue;

    public static VolleyManager getsInstance() {
        if (sInstance == null){
            sInstance = new VolleyManager();
        }

        return sInstance;
    }

    private VolleyManager(){}

    public void postRequestJson(Context context, String url, JSONObject jsonBody, Response.Listener<String> stringListener, Response.ErrorListener errorListener){
        if (mRequestQueue == null){
            defineRequestQueue(context);
        }

        final String requestBody = jsonBody.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, stringListener, errorListener){
            @Override
            public byte[] getBody() throws AuthFailureError { //Body
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };


        mRequestQueue.add(stringRequest);
    }

    public void getRequestString(Context context, String url, Response.Listener<String> stringListener, Response.ErrorListener errorListener){
        if (mRequestQueue == null){
            defineRequestQueue(context);
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, stringListener, errorListener);

        mRequestQueue.add(stringRequest);
    }

    public void getRequestJSON(Context context, String url, Response.Listener<JSONObject> jsonListener, Response.ErrorListener errorListener){
        if (mRequestQueue == null){
            defineRequestQueue(context);
        }

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, jsonListener, errorListener);

        mRequestQueue.add(jsonObjRequest);
    }

    private void defineRequestQueue(Context context) {
        // Instantiate the cache
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024); // 1MB cap

        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());

        // Instantiate the RequestQueue with the cache and network.
        mRequestQueue = new RequestQueue(cache, network);

        // Start the queue
        mRequestQueue.start();



    }


}
