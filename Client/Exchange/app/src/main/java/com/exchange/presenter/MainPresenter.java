package com.exchange.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.volley.Response;
import com.exchange.model.Item;
import com.exchange.model.server.Server;
import com.exchange.model.User;
import com.exchange.model.server.VolleyManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eitan on 16/01/2017.
 */

public class MainPresenter {
    private VolleyManager mVolleyManager;

    public MainPresenter(){
        mVolleyManager = VolleyManager.getsInstance();
    }

    /* VIEW ACCESS */
    public void getLocalisation(@NonNull Context context, @NonNull String language, @NonNull Response.Listener<String> stringListener, @NonNull Response.ErrorListener errorListener){
        getString(context, Server.getLocalisationUrl(language), stringListener, errorListener);
    }

    public void getItems(@NonNull Context context, @Nullable Integer itemId, @Nullable Long userId, @Nullable String itemName, @Nullable String itemType, @Nullable String itemCondition, @Nullable String itemLocation, @NonNull Response.Listener<String> stringListener, @NonNull Response.ErrorListener errorListener) {
        getString(context, Server.getItemsUrl(itemId, userId, itemName, itemType, itemCondition, itemLocation), stringListener, errorListener);
    }

    public void getAllItems(@NonNull Context context, @NonNull Response.Listener<String> stringListener, @NonNull Response.ErrorListener errorListener){
        getString(context, Server.viewAllItemsUrl(), stringListener, errorListener);
    }

    public void deleteUser(@NonNull Context context, @NonNull User user, @NonNull Response.Listener<String> stringListener, @NonNull Response.ErrorListener errorListener){
        getString(context, Server.deleteUserUrl(user), stringListener, errorListener);
    }

    public void deleteItem(@NonNull Context context, @NonNull Item item, @NonNull Response.Listener<String> stringListener, @NonNull Response.ErrorListener errorListener){
        getString(context, Server.deleteItemUrl(item), stringListener, errorListener);
    }

    public void addItem(@NonNull Context context, @NonNull Item item, @NonNull Response.Listener<String> stringListener, @NonNull Response.ErrorListener errorListener){
        try {
            JSONObject itemJO = new JSONObject();
            itemJO.put("itemId", String.valueOf(item.getId()));
            itemJO.put("userId", String.valueOf(item.getUserId()));
            itemJO.put("itemName", item.getName());
            itemJO.put("itemType", item.getType());
            itemJO.put("itemCondition", item.getCondition());
            itemJO.put("itemLocation", item.getLocation());
            itemJO.put("itemImage", item.getImageString());

            postMap(context, Server.getAddItemUrl(), itemJO, stringListener, errorListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* MODEL ACCESS */
    private void getString(Context context, String url, Response.Listener<String> stringListener, Response.ErrorListener errorListener){
        mVolleyManager.getRequestString(context, url, stringListener, errorListener);
    }

    private void getJson(Context context, String url, Response.Listener<JSONObject> jsonListener, Response.ErrorListener errorListener){
        mVolleyManager.getRequestJSON(context, url, jsonListener, errorListener);
    }

    private void postMap(Context context, String url, JSONObject jsonBody, Response.Listener<String> stringListener, Response.ErrorListener errorListener){
        mVolleyManager.postRequestJson(context, url, jsonBody, stringListener, errorListener);
    }
}
