package com.exchange.model.camera;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by eitan on 17/01/2017.
 */

public class OrientationUtils {

    public int getOrientationFromDeviceOrientation(Camera camera) {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(0, cameraInfo);
        int cameraRotation = cameraInfo.orientation; //The angle that the camera image needs to be rotated clockwise so it shows correctly on the display in its natural orientation.
        return cameraRotation;
    }

    public int getOrientationFromMetaData(byte[] data) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            try {
                InputStream dataInputStream = new ByteArrayInputStream(data);
                ExifInterface exifInterface = new ExifInterface(dataInputStream);
                return exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED); //falls back to CameraExif if failed (which returns NO_ORIENTATION_DATA if no meta-data found)
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        CameraExif cameraExif = new CameraExif();
        return cameraExif.getOrientation(data); //returns NO_ORIENTATION_DATA if couldn't find meta-data
    }

    public Bitmap rotateBitmap(Bitmap pictureBitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        Log.d("ahmed", "rotateBitmap degrees = " + degrees);
        return Bitmap.createBitmap(pictureBitmap, 0, 0, pictureBitmap.getWidth(), pictureBitmap.getHeight(), matrix, true);
    }
}
