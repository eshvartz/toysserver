package com.exchange.model.camera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by eitan on 16/01/2017.
 */

public class Base64Bitmap {
    private Bitmap mBitmap;

    /* Constructors */
    public Base64Bitmap(Bitmap bitmap){
        mBitmap = bitmap;
    }

    public Base64Bitmap(String base64String){
        decode(base64String);
    }

    /* Get representations */
    public String getBase64String(){
        return encode();
    }

    public Bitmap getBitmap(){
        return mBitmap;
    }

    /* Utilities */
    private void decode(String base64String) {
        byte[] imageAsBytes = Base64.decode(base64String.getBytes(), Base64.DEFAULT);
        mBitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);

        if (mBitmap == null){
            Log.e(TAG, "Bitmap decode was not successful. Input base64String = " + base64String);
        }
    }

    private String encode(){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }


}
