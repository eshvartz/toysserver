package com.exchange.model.camera;

import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * Created by eitan on 17/01/2017.
 */

public class ExchangeCamera {
    private static final String TAG = "ExchangeCamera";

    public static final int NO_ORIENTATION_DATA = -1;

    private Camera mCamera;
    private SurfaceHolder mSurfaceHolder;
    private boolean mPreviewRunning;
    private Camera.PictureCallback mPictureCallback;

    public ExchangeCamera(SurfaceView cameraSV){
        setupCamera(cameraSV);
    }

    public void takePicture(Camera.PictureCallback pictureCallback){
        mCamera.takePicture(null, null, pictureCallback);
    }

    private void setupCamera(SurfaceView cameraSV) {
        mSurfaceHolder = cameraSV.getHolder();
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                mCamera = Camera.open();
                mCamera.setDisplayOrientation(90);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                setPreview();
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (mCamera != null) {
                    mCamera.cancelAutoFocus();
                    mCamera.stopPreview();
                    mCamera.release();
                    mCamera = null;
                }
            }
        });
    }

    public void setPreview() { //camera must show what's it's going to picture before taking image or else it crashes
        if(mPreviewRunning){
            mCamera.stopPreview();
        }
        Camera.Parameters camParams = mCamera.getParameters();
        Camera.Size size = camParams.getSupportedPreviewSizes().get(0);
//        if (squareBitmapRequested){
//            for (Camera.Size tmpSize : camParams.getSupportedPreviewSizes()){
//                Log.d("ahmed", "tmpSize = " + tmpSize.width + ", " + tmpSize.height);
//                if (tmpSize.width == tmpSize.height){
//                Log.d("ahmed", "sqr tmpSize found!");
//                    size.width = tmpSize.width;
//                    size.height = tmpSize.height;
//                    break;
//                }
//            }
//        }

        camParams.setPreviewSize(size.width, size.height);

        mCamera.setParameters(camParams);
        try {
            mCamera.setPreviewDisplay(mSurfaceHolder);
            mCamera.startPreview();
            mPreviewRunning = true;
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
