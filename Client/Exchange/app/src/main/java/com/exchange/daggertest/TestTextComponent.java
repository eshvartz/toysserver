package com.exchange.daggertest;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by eitan on 19/01/2017.
 */

@Singleton
@Component(modules = {TextModule1.class, TextModule2.class})
public interface TestTextComponent {
    String provideTestText1(String testText);
    String provideTestText2(String testText);

    void inject(DaggerActivity daggerActivity);
}
