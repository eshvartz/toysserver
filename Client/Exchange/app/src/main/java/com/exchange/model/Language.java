package com.exchange.model;

/**
 * Created by eitan on 16/01/2017.
 */

public class Language {
    public static final String HEBREW = "Hebrew";
    public static final String ENGLISH = "English";
}
