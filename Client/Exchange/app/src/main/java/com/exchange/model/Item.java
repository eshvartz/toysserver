package com.exchange.model;

import android.graphics.Bitmap;

import com.exchange.model.camera.Base64Bitmap;

/**
 * Created by eitan on 16/01/2017.
 */

public class Item {
    private Integer mId;
    private Long mUserId;
    private String mName;
    private String mType;
    private String mCondition;
    private String mLocation;
    private Base64Bitmap mImage;

    public Item(int id, long userId, String name, String type, String condition, String location, String base64BitmapString){
        this(id, userId, name, type, condition, location, new Base64Bitmap(base64BitmapString));
    }

    public Item(int id, long userId, String name, String type, String condition, String location, Bitmap bitmap){
        this(id, userId, name, type, condition, location, new Base64Bitmap(bitmap));
    }

    public Item(int id, long userId, String name, String type, String condition, String location, Base64Bitmap base64Bitmap){
        mId = id;
        mUserId = userId;
        mName = name;
        mType = type;
        mCondition = condition;
        mLocation = location;
        mImage = base64Bitmap;
    }

    public int getId() {
        return mId;
    }

    public long getUserId() {
        return mUserId;
    }

    public String getName() {
        return mName;
    }

    public String getType() {
        return mType;
    }

    public String getCondition() {
        return mCondition;
    }

    public String getLocation() {
        return mLocation;
    }

    public String getImageString() {
        return mImage.getBase64String();
    }

    public Bitmap getImageBitmap() {
        return mImage.getBitmap();
    }
}
