package com.exchange.daggertest;

import android.util.Log;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by eitan on 19/01/2017.
 */

@Module
public class TextModule1 {
    @Provides
    @Singleton //always return the same instance (TestText String)
    String provideTestText1(String testText){
        Log.d("ahmed","1");
        return "Dagger text 1: " + testText;
    }
}
