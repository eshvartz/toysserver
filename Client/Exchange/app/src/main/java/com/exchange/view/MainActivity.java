package com.exchange.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.exchange.R;
import com.exchange.model.Item;
import com.exchange.model.Language;
import com.exchange.model.camera.Base64Bitmap;
import com.exchange.presenter.MainPresenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private MainPresenter mMainPresenter;
    private TextView mTestTV;
    private ImageView mTestIV;
    private Base64Bitmap mSnapshotBitmap;

    private int tmpImageRandom = (int)(Math.random()*1000);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainPresenter = new MainPresenter();
        mTestTV = (TextView) findViewById(R.id.test_tv);
        mTestIV = (ImageView) findViewById(R.id.test_iv);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.transl1_btn:
                mMainPresenter.getLocalisation(MainActivity.this, Language.HEBREW, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mTestTV.setText(response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mTestTV.setText(error.getLocalizedMessage());
                    }
                });
                break;
            case R.id.transl2_btn:
                mMainPresenter.getLocalisation(MainActivity.this, Language.ENGLISH, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mTestTV.setText(response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mTestTV.setText(error.getLocalizedMessage());
                    }
                });
                break;
            case R.id.download_pic_btn:
                mMainPresenter.getItems(MainActivity.this, tmpImageRandom, 1l, null, null, null, null, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray responseJA = new JSONArray(response);
                            JSONObject testItemJO = (JSONObject) responseJA.get(0);
                            String base64bitmapString = testItemJO.optString("image");
                            String testItemName = testItemJO.optString("name");
                            mTestTV.setText(testItemName);
                            mTestIV.setImageBitmap(new Base64Bitmap(base64bitmapString).getBitmap());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mTestTV.setText(error.getLocalizedMessage());
                    }
                });

                tmpImageRandom = (int)(Math.random()*1000);
                break;
            case R.id.upload_pic_btn:
                if (mSnapshotBitmap == null){
                    mTestTV.setText("Snapshot empty..");
                    return;
                }

                Item item = new Item(tmpImageRandom, 1, "clientTestName", "clientTestType", "clientTestCondition", "clientTestLocation", mSnapshotBitmap.getBase64String());
                mMainPresenter.addItem(MainActivity.this, item, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mTestTV.setText(response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mTestTV.setText(error.getLocalizedMessage());
                    }
                });
                break;
            case R.id.new_item_activity_btn:
                Intent newItemIntent = new Intent(MainActivity.this, CameraActivity.class);
                startActivityForResult(newItemIntent, CameraActivity.REQUEST_CODE_TAKE_SNAPSHOT);
                break;
            case R.id.new_sqr_item_activity_btn:
                Intent newSqrItemIntent = new Intent(MainActivity.this, CameraActivity.class);
                newSqrItemIntent.putExtra(CameraActivity.INTENT_EXTRA_REQUETS_SQUARE_SNAPSHOT, true);
                startActivityForResult(newSqrItemIntent, CameraActivity.REQUEST_CODE_TAKE_SNAPSHOT);
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CameraActivity.REQUEST_CODE_TAKE_SNAPSHOT && resultCode == RESULT_OK){
            Bitmap snapshotBitmap = data.getParcelableExtra(CameraActivity.INTENT_EXTRA_SNAPSHOT);
            mSnapshotBitmap = new Base64Bitmap(snapshotBitmap);
        }

    }
}
