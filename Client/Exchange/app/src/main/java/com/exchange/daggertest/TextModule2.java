package com.exchange.daggertest;

import android.util.Log;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by eitan on 19/01/2017.
 */

@Module
public class TextModule2 {
    @Provides
    @Singleton //always return the same instance (TestText String)
    String provideTestText2(String testText){
        Log.d("ahmed","2");
        return "Dagger text 2: " + testText;
    }
}
