var express = require('express') //require is nodeJS import. this one imports express.js module
var app = express() //app is now the access point for all ExpressJS functionality 
//var listen_port = 3000; //listening to incoming requests on this port (base url is currently localhost:3000)
var listen_port = 80; //"no-port" is actually port 80 which requires root permissions to execute (therefor, run this script using 'sudo node app.js')

//Enable POST request body parsing
var bodyParser = require('body-parser');
app.use(bodyParser({limit: '16mb'})); //limits post requests to 16MB (database uses MEDIUMTEXT for images which is 16MB in size limit)
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

//Define general port listening
app.listen(listen_port, function () { //listen defines what connection ports will be "caught" by this specific app
  console.log('Listening on port ' + listen_port)
})

console.log('TODO:: Client responsible for Base64 inflate/deflate & for sending all users items when calling *deleteUser*');


//**********************//
//***** JSON Tools *****//
//**********************//
var jsonModule = require("./exchangeModules/json_module");
app.get('/item/getItem', function(req, res) {
        jsonModule.getItem(req, res, "items/item1.json");
})

//*************************//
//***** MySql - items *****//
//*************************//
var itemsDbModule = require("./exchangeModules/item_db_module");

app.post('/items/addItem', function (req, res) { // POST: Url | localhost:3000/items/addItem , Header | Content-Type=application-json , Body |{"itemId": 2131, "userId": 321, "itemName": "testName", "itemType": "testType", "itemCondition": "testCondition", "itemLocation": "testLocation", "itemImage": "notyet_base64"}
        itemsDbModule.addItemToTable(req, res);
})

app.get('/items/getItemsFromTable', function(req, res) { // http://localhost:3000/items/getItemsFromTable?itemId=12 OR http://localhost:3000/items/getItemsFromTable?itemId=12&itemName='name' (any combination of itemId, userId, itemName, itemType, itemCondition, itemLocation )
        itemsDbModule.getItemsFromTable(req, res);
})

app.get('/items/viewItemTable', function(req, res) { // http://localhost:3000/items/viewItemTable
        itemsDbModule.viewItemTable(req, res);
})

app.get('/item/deleteItem', function(req, res) { // http://localhost:3000/items/deleteItem?itemId=2
        itemsDbModule.deleteItem(req, res);
})

app.get('/items/deleteUser', function(req, res) { // http://localhost:3000/items/deleteUser?itemIds=2,4 (deletes all items that belong to a user (client responsible for sending array of itemIds (key: itemIds)))
        itemsDbModule.deleteUser(req, res);
})

//********************************//
//***** MySql - localisation *****//
//********************************//
var localisationDbModule = require("./exchangeModules/localisation_db_module");

app.get('/localisation/getLocalisationFromTable', function(req, res) { // http://localhost:3000/localisation/getLocalisationFromTable?language=<language>
        localisationDbModule.getLocalisationFromTable(req, res);
})

//*******************//
//***** Cleanup *****//
//*******************//
// loads module and registers app specific cleanup callback...
var cleanup = require('./exchangeModules/cleanup_module').Cleanup(myCleanup);
//var cleanup = require('./cleanup').Cleanup(); // will call noOp

// defines app specific callback...
function myCleanup() {
  console.log('Cleaning up..');
  itemsDbModule.endMySqlConnection();
  localisationDbModule.endMySqlConnection();
};

// All of the following code is only needed for test demo

// Prevents the program from closing instantly
process.stdin.resume();

// Emits an uncaught exception when called because module does not exist
function error() {
  console.log('error');
  var x = require('');
};


