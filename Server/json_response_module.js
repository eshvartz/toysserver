var http = require('http');

exports.return_test_json = function (req, res) {
        res.setHeader('Content-Type', 'application/json');
	res.send(JSON.stringify({ a: 2 }));
}

exports.return_json_from_file = function (req, res, filepath) {
	var fileSystem = require('fs');
	fileSystem.readFile(filepath, 'utf8', function(err, contents) {
		console.log("fileSystem | readFile (" + filepath + ")");
		res.setHeader('Content-Type', 'application/json');
        	res.send(contents);        
		
		//The following returns the JSON encased with String parenthisis 	
		//res.send(JSON.stringify(contents)); //returns the json in a string format
	});
}
