package com.exchange.model.server;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.exchange.model.Item;
import com.exchange.model.User;

/**
 * Created by eitan on 16/01/2017.
 */

public class Server {
    private static final String BASE_URL  = "http://localhost/";
    private static final String URL_LOCALISATION  = "localisation/getLocalisationFromTable?language=%s";

    private static final String URL_ITEMS_ADD_ITEM   = "items/addItem"; //filter by any combination of available filters

    private static final String URL_ITEMS_GET_ITEMS_BY_FILTER   = "items/getItemsFromTable?%s"; //filter by any combination of available filters
    private static final String GET_ITEMS_FILTER_ITEM_ID        = "&itemId=%s";
    private static final String GET_ITEMS_FILTER_USER_ID        = "&userId=%s";
    private static final String GET_ITEMS_FILTER_ITEM_NAME      = "&itemName=%s";
    private static final String GET_ITEMS_FILTER_ITEM_TYPE      = "&itemType=%s";
    private static final String GET_ITEMS_FILTER_ITEM_CONDITION = "&itemCondition=%s";
    private static final String GET_ITEMS_FILTER_ITEM_LOCATION  = "&itemLocation=%s";

    private static final String URL_ITEMS_VIEW_ITEMS  = "items/viewItemTable";

    private static final String URL_ITEMS_DELETE_USER  = "items/deleteUser?itemIds=%s";//2,4,7
    private static final String URL_ITEMS_DELETE_ITEM  = "items/deleteItemUrl?itemId=%s"; //2

    public static String getAddItemUrl(){
        return BASE_URL + URL_ITEMS_ADD_ITEM;
    }

    public static String getLocalisationUrl(@NonNull String language){
        return String.format(BASE_URL + URL_LOCALISATION, language);
    }

    public static String getItemsUrl(@Nullable Integer itemId, @Nullable Long userId, @Nullable String itemName, @Nullable String itemType, @Nullable String itemCondition, @Nullable String itemLocation){
        StringBuilder sb = new StringBuilder();
        if (itemId != null){
            sb.append(String.format(GET_ITEMS_FILTER_ITEM_ID, itemId));
        }
        if (userId != null){
            sb.append(String.format(GET_ITEMS_FILTER_USER_ID, userId));
        }
        if (!TextUtils.isEmpty(itemName)){
            sb.append(String.format(GET_ITEMS_FILTER_ITEM_NAME, itemName));
        }
        if (!TextUtils.isEmpty(itemType)){
            sb.append(String.format(GET_ITEMS_FILTER_ITEM_TYPE, itemType));
        }
        if (!TextUtils.isEmpty(itemCondition)){
            sb.append(String.format(GET_ITEMS_FILTER_ITEM_CONDITION, itemCondition));
        }
        if (!TextUtils.isEmpty(itemLocation)){
            sb.append(String.format(GET_ITEMS_FILTER_ITEM_LOCATION, itemLocation));
        }
        String filterString = sb.toString();

        return String.format(BASE_URL + URL_ITEMS_GET_ITEMS_BY_FILTER, filterString);
    }

    public static String viewAllItemsUrl(){ //This one will probably be removed
        return BASE_URL + URL_ITEMS_VIEW_ITEMS;
    }

    public static String deleteUserUrl(@NonNull User user){ //Atm, deleting a user is deleting all of their items
        StringBuilder sb = new StringBuilder();
        for (Item item : user.getItems()){
            if (sb.length() > 0){
                sb.append(",");
            }
            sb.append(item.getId());
        }
        String userItems = sb.toString();
        return String.format(BASE_URL + URL_ITEMS_DELETE_USER, userItems);
    }

    public static String deleteItemUrl(@NonNull Item item){
        return String.format(BASE_URL + URL_ITEMS_DELETE_ITEM, item.getId());
    }
}
