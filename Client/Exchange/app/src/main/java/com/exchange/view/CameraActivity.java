package com.exchange.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.exchange.R;
import com.exchange.model.DeviceUtils;
import com.exchange.presenter.CameraPresenter;

/**
 * Created by eitan on 17/01/2017.
 */

public class CameraActivity extends AppCompatActivity {
    private static final String TAG = "CameraActivity";

    public static final int REQUEST_CODE_TAKE_SNAPSHOT = 1;
    public static final String INTENT_EXTRA_SNAPSHOT = "intent_extra_bitmap_snapshot"; //returned bitmap bundled under this key
    public static final String INTENT_EXTRA_REQUETS_SQUARE_SNAPSHOT = "intent_extra_request_extra_snapshot"; //calling activity can request a square picture by adding this extra

    private CameraPresenter mCameraPresenter;
    private SurfaceView mCameraSV;
    private ImageView mCameraImage;
    private Button mSnapShotApprovalButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestFullScreen();

        setContentView(R.layout.activity_camera);

        boolean squareBitmapRequested = getIntent().getBooleanExtra(INTENT_EXTRA_REQUETS_SQUARE_SNAPSHOT, false);

        setupSurfaceView(squareBitmapRequested);

        mCameraPresenter = new CameraPresenter(mCameraSV, squareBitmapRequested);

        mCameraImage = (ImageView)findViewById(R.id.camera_image);

        setupSnapshotButton(squareBitmapRequested);

        setupSnapshotApprovalButton();
    }

    private void setupSurfaceView(boolean squareBitmapRequested) {
        //find camera SurfaceView
        mCameraSV = (SurfaceView) findViewById(R.id.camera_sv);

        //add black blockers if required (image is also cropped to be square)
        if (squareBitmapRequested) {
            Point displayDimensions = DeviceUtils.getDeviceDimensions(this);
            int blockerHeight = (displayDimensions.y - displayDimensions.x) / 2;

            View topBlocker = (View) findViewById(R.id.square_cam_blocker_top);
            View bottomBlocker = (View) findViewById(R.id.square_cam_blocker_bottom);

            Log.d("ahmed","blocker heigth " + blockerHeight);
            topBlocker.getLayoutParams().height = blockerHeight;
            bottomBlocker.getLayoutParams().height = blockerHeight;
        }
    }

    private void setupSnapshotButton(final boolean squareBitmapRequested) {
        Button snapShotButton = (Button)findViewById(R.id.snapshot_btn);
        snapShotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCameraPresenter.takePicture(new CameraPresenter.UserPictureListener() {
                    @Override
                    public void onPictureTaken(Bitmap pictureBitmap) {
                        //set bitmap in preview thumbnail
                        mCameraImage.setImageBitmap(pictureBitmap);
                        mSnapShotApprovalButton.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError(String errorString) {
                        Log.e(TAG, errorString);
                    }
                });
            }
        });
    }

    private void setupSnapshotApprovalButton() {
        mSnapShotApprovalButton = (Button)findViewById(R.id.approve_snapshot_btn);
        mSnapShotApprovalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Check for last snapshot, cancel if non available
                BitmapDrawable lastSnapshotDrawable = ((BitmapDrawable)mCameraImage.getDrawable());
                if (lastSnapshotDrawable == null){
                    setResult(RESULT_CANCELED);
                    finish();
                    return;
                }

                //Return last snapshot to calling Activity
                Bitmap lastSnapshot = lastSnapshotDrawable.getBitmap();
                Intent resultIntent = new Intent();
                resultIntent.putExtra(INTENT_EXTRA_SNAPSHOT, lastSnapshot);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
    }

    private void requestFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

}
