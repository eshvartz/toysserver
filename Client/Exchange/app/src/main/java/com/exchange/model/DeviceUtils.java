package com.exchange.model;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;

/**
 * Created by eitan on 18/01/2017.
 */

public class DeviceUtils {

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static Point getDeviceDimensions(Activity activity){
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;

    }
}
