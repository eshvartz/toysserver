//***************//
//***** API *****//
//***************//
var http = require('http');

exports.viewItemTable = function (req, res) {
	console.log("db_module | viewItemTable called.");
	
	mySqlConnection.query('SELECT * from items_table LIMIT 20', function(err, rows, fields) {		
		  if (!err) {
		    console.log('The solution is: ', rows);
		  } else {
		    console.log('Error while performing viewItemTable. err = ' + err);
		  }
		
		res.send(rows);
	});
}

exports.addItemToTable = function (req, res, itemId, userId, itemName, itemType, itemCondition, itemLocation, itemImage) {
	console.log("db_module | addItemToTable called.");
    
    var itemId = req.body.itemId;
    var userId = req.body.userId;
    var itemName = req.body.itemName;
    var itemType = req.body.itemType;
    var itemCondition = req.body.itemCondition;
    var itemLocation = req.body.itemLocation;
    var itemImage = req.body.itemImage;

    if (!itemId || !userId || !itemName || !itemType || !itemCondition || !itemLocation || !itemImage){
        res.status(400); //bad/malformed request
        res.send('Please verify all parameters are defined correctly.');
        return;
    }

	var itemDescriptor = {id: itemId, userId: userId, name: itemName, type: itemType, itemCondition: itemCondition, location: itemLocation, image: itemImage};
	mySqlConnection.query('INSERT INTO `itemsdb`.`items_table` SET ?', itemDescriptor, function(err, rows, fields) {		
		  if (!err) {
		    console.log('DB Table: ', rows);
		  } else {
		  	var errString = 'Error while performing addItemToTable. err = ' + err;
		    console.log(errString);
		    res.status(400); //bad request
		    res.send(errString);
		  }
		
		res.send(rows);
	});
}

//get all items, filtered by one of the following (the rest should be null): itemId, userId, itemType, itemCondition, itemLocation
exports.getItemsFromTable = function (req, res) {
	console.log("db_module | getItemsFromTable called.");
	var itemId = req.query.itemId;
    var userId = req.query.userId;
    var itemName = req.query.itemName;
    var itemType = req.query.itemType;
    var itemCondition = req.query.itemCondition;
    var itemLocation = req.query.itemLocation;

    var baseQuery = 'SELECT * FROM `itemsdb`.`items_table` WHERE ';
    var paramArray = [];
    var itemIdQuery = '';
    if (itemId){
    	itemIdQuery = 'id = \'' + itemId + '\'';
    	paramArray.push(itemIdQuery);
    }
	var userIdQuery = '';
    if (userId){
    	userIdQuery = 'userId = \'' + userId + '\'';
    	paramArray.push(userIdQuery);
 	}
    var itemNameQuery = '';
    if (itemName){
    	itemNameQuery = 'itemName = \'' + itemName + '\'';
    	paramArray.push(itemNameQuery);
    }
    var itemTypeQuery = '';
    if (itemType){
    	itemTypeQuery = 'itemType = \'' + itemType + '\'';
    	paramArray.push(itemTypeQuery);
    }
    var itemConditionQuery = '';
    if (itemCondition){
    	itemConditionQuery = 'itemCondition = \'' + itemCondition + '\'';
    	paramArray.push(itemConditionQuery);
    }
    var itemLocationQuery = '';
    if (itemLocation){
    	itemLocationQuery = 'itemLocation = \'' + itemLocation + '\'';
    	paramArray.push(itemLocationQuery);
    }


    var mySqlQuery = baseQuery;
    for (var i=0; i<paramArray.length; i++){
    	if (i > 0){
    		mySqlQuery += ' AND ';
    	}
    	mySqlQuery += paramArray[i];
    }

    console.log(mySqlQuery);
	/*
    var mySqlQuery = 'SELECT * FROM `itemsdb`.`items_table` WHERE id = \'' + itemId
      + '\' OR userId = \'' + userId
      + '\' OR name = \'' + itemName
      + '\' OR type = \'' + itemType
      + '\' OR itemCondition = \'' + itemCondition 
      + '\' OR location = \'' + itemLocation
      + '\' ORDER BY id';
    */
    console.log(mySqlQuery);

	mySqlConnection.query(mySqlQuery, function(err, rows, fields) {		
		  if (err) {		  
		  	var errString = 'Error while performing getItemsFromTable. err = ' + err;
		    console.log(errString);
		    res.status(400); //bad request
		    res.send(errString);
		  }
		
		res.send(rows);
	});

}

exports.deleteItem = function (req, res) {
	console.log("db_module | deleteItem called.");
	var itemId = req.query.itemId;

	mySqlConnection.query('DELETE from items_table WHERE id = ' + itemId, function(err, rows, fields) {
		  if (!err) {
		    console.log('The solution is: ', rows);
		  } else {
		    console.log('Error while performing viewItemTable. err = ' + err);
		  }
		
		res.send(rows);
	});
}

exports.deleteUser = function (req, res) { //a user currently only exists as a field in table items so we just delete all of their items
	console.log("db_module | deleteUser called.");
	var itemIds = req.query.itemIds;

	var mySqlQuery = 'DELETE from items_table WHERE id IN (' + itemIds + ')';
	console.log(mySqlQuery);
	mySqlConnection.query(mySqlQuery, function(err, rows, fields) {
		  if (!err) {
		    console.log('The solution is: ', rows);
		  } else {
		    console.log('Error while performing viewItemTable. err = ' + err);
		  }
		
		res.send(rows);
	});
}
//*************************//
//***** MySql - Setup *****//
//*************************//

//Verify you got a running MySql server
//Default credentials for local machine: root@localhost: B(Okxpd#u1lM
//New credentials for local machine: root@localhost: mypass15

console.log("Defining MySql connection..");
var LOCAL_SQL_SERVER_PORT = 3306;
var LOCAL_SQL_SERVER_HOST = 'localhost';
var LOCAL_SQL_SERVER_USER = 'root';
var LOCAL_SQL_SERVER_PASS = 'mypass15';
var LOCAL_SQL_SERVER_DATABASE = 'itemsdb';


var mySql = require('mysql')
var mySqlConnection = mySql.createConnection({
	port: LOCAL_SQL_SERVER_PORT,  //should either be the default port (3306) or the same port as the one our app.js is listening to (3000)
	host: LOCAL_SQL_SERVER_HOST,
	user: LOCAL_SQL_SERVER_USER,
	password: LOCAL_SQL_SERVER_PASS,
	database: LOCAL_SQL_SERVER_DATABASE
});

connectionStartHandleErrors();

//Print table on connection
/*
mySqlConnection.query('SELECT * from items_table', function(err, rows, fields) {
	if (!err) {
    	console.log('DB Table: ', rows);
 	} else {
    	console.log('Error while performing Query. err = ' + err);
	}
});
*/

//**************************************//
//***** MySql - Connection Helpers *****//
//**************************************//

function connectionStartHandleErrors(){
	mySqlConnection.connect(function(err){
		if (err){
			console.log("MySql connection failed. err = " + err);
			console.log("Did you remember to activate MySql server from System Preferences?");
		} else {
			console.log("MySql (Items) connection succesful.");
		}
	});
}

//From docs: Closing the connection is done using end() which makes sure all remaining queries are executed before sending a quit packet to the mysql server.
function connectionEndHandleErrors(){
	mySqlConnection.end();
	console.log('Connection closed.'); //TODO: i'm not sure connection really closes :/ verify that later on..
		// mySqlConnection.end(function(endErr){
		// 	if (endErr) {
		//     	console.log('Error closing connection. err = ' + endErr);
		// 	}else{
		// 		console.log('Connection closed.');
		// 	}
		// });
}

exports.endMySqlConnection = connectionEndHandleErrors;